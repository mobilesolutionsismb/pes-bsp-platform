#### ISMB Release Notes

This branch is based on 

* Poky: yocto-2.2.1 (morty-16.0.1)
* FSL Community BSP: 2.2
* QT: 5.7.0
* OS: Ubuntu 16.04 64-bit

# RCS BSP Platform

## Pre-requisites

Install the following packages:

```console
$: sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
xz-utils debianutils iputils-ping libsdl1.2-dev xterm screen
```

```console
$: sudo apt-get install autoconf libtool libglib2.0-dev libarchive-dev python-git \
xterm sed cvs subversion coreutils texi2html docbook-utils python-pysqlite2 \
help2man make gcc g++ desktop-file-utils libgl1-mesa-dev libglu1-mesa-dev \
mercurial automake groff curl lzop asciidoc u-boot-tools dos2unix mtd-utils pv \
libncurses5 libncurses5-dev libncursesw5-dev libelf-dev
```

## Download sources from repo
Configure git
```console
$: git config --global user.name "Your Name"
$: git config --global user.email "Your Email"
```

Install the `repo` utility:

```console
$: mkdir ~/bin
$: curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$: chmod a+x ~/bin/repo
```

Download the BSP source:

```console
$: PATH=${PATH}:~/bin
$: mkdir ~/ismb-pes-morty
$: cd ~/ismb-pes-morty
$: repo init -u ssh://git@bitbucket.org/mobilesolutionsismb/pes-bsp-platform.git -b master
$: repo sync -j4
```

The source code is checked out at `ismb-pes-morty/sources`.

At the end of the commands you have every metadata you need to start work with.

## Source the environment

Everytime you open a new terminal shell, you need to apply the environmental variable, before using bitbake:

```console
$: MACHINE=imx6ul-var-dart DISTRO=fslc-framebuffer-pes source ./setup-environment build_fb			(for console only image)
$: MACHINE=imx6ul-var-dart DISTRO=fslc-wayland-pes source ./setup-environment build_fb				(for graphical frontend image)
```

## Build the image

To start a simple image build:

```console
$: bitbake fsl-core-pes 				(for console only image)
$: bitbake fsl-pes-qt5  				(for graphical frontend image)
```

At the end of the commands you have the image at `.build_fb/tmp/deploy/images/imx6ul-var-dart/`

## Build the SDK

To generate a standalone SDK compatible with the image generated:

```console
$: bitbake fsl-core-pes -c populate_sdk			(for console only image)
$: bitbake fsl-pes-qt5 -c populate_sdk			(for graphical frontend image)
```

## Write the Extended SD Card

Using this command, you can write the image on the SD card, with all the needed script to transfer it on the eMMC.
Plug-in the SD card to your Linux HOST PC, run dmesg and see which device is added (i.e. /dev/sdX or /dev/mmcblkX) 

Modify the file in sources/meta-ismb-pes/scripts/create_ext_sd.sh and set the variable YOCTO_DEFAULT_IMAGE (line 23) to reflect the image you want to write on SD.  
Save the file and run the following commands:

```console
$: cd ~/ismb-pes-morty
$: sudo MACHINE=imx6ul-var-dart sources/meta-ismb-pes/scripts/create_ext_sd.sh /dev/sdX
```

**WARNING! You should substitute the X in the command with the right letter of the SD card volume (ie. /dev/sdb or /dev/sdc)**

## Writing SD image on eMMC

Start the device, booting from SD. Once at the prompt use the command:

```console
imx6ul-var-dart login: root
root@imx6ul-var-dart:~# install_emmc
```